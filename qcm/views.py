import csv
import random
import time

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

from django.core import serializers
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.views import generic
from django.db.models import Count
from django_tex.shortcuts import render_to_pdf

from qcm.models import Question, Quiz, QuestionOrder, ImportRecord, Submission, Answer, Category, Level
from .constants import *
from .forms import QuestionForm, QuizForm
import json


class IndexView(generic.TemplateView):
    '''
    Page demandant de rentrer un code pour accéder à un quiz en tant qu'étudiant
    '''
    template_name = 'qcm/index.html'

    def post(self, request):
        code = request.POST['code']
        if code.isdigit():
            if int(code) in list(Quiz.objects.all().values_list('code', flat=True)):
                return redirect(reverse_lazy('start_quiz', kwargs={'code': code}))

        messages.add_message(request,
                             messages.ERROR,
                             "Vérifiez le code qui vous a été donné.", extra_tags="Code incorrect")
        return redirect(reverse_lazy('index'))


# Question


class CreateQuestionView(LoginRequiredMixin, generic.CreateView):
    '''
    Créer une question
    '''
    model = Question
    form_class = QuestionForm

    def get_success_url(self):
        return reverse_lazy('create_question')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(CreateQuestionView, self).form_valid(form)


class UpdateQuestion(LoginRequiredMixin, generic.UpdateView):
    '''
    Modification d'une question
    '''
    model = Question
    form_class = QuestionForm

    def get_success_url(self):
        return self.request.POST.get('next', '/')


class ListQuestions(LoginRequiredMixin, generic.ListView):
    '''
    Lister toutes les questions
    '''
    model = Question

    def get_queryset(self):
        return Question.objects.select_related('category').prefetch_related('tags').all()


class ListQuestionsByTag(LoginRequiredMixin, generic.ListView):
    '''
    Lister les questions contenant un tag donnée
    '''
    model = Question

    def get_queryset(self):
        tag = self.kwargs['tag']
        return Question.objects.filter(tags__name__in=[tag])


class ListQuestionsByCategory(LoginRequiredMixin, generic.ListView):
    '''
    Liste les questions appartenant à une catégorie
    '''
    model = Question

    def get_queryset(self):
        category = self.kwargs['category']
        return Question.objects.select_related('category').prefetch_related('tags').filter(category__code=category)

    def get_context_data(self, **kwargs):
        context = super(ListQuestionsByCategory, self).get_context_data(**kwargs)
        # todo: vérifier si ca sert encore
        verbose_category = dict(CHOICES_CATEGORY)
        context['category'] = verbose_category[self.kwargs.get('category')]
        # todo : filter par user loggé
        context['quiz_list'] = Quiz.objects.all()
        return context


class ListQuestionsByImportRecord(LoginRequiredMixin, generic.ListView):
    '''
    Lister toutes les questions d'une opération d'importation de questions
    '''
    model = ImportRecord

    def get_queryset(self):
        import_record = self.kwargs['import_record']
        return Question.objects.select_related('category').prefetch_related('tags').filter(import_record=import_record)

    def get_context_data(self, **kwargs):
        context = super(ListQuestionsByImportRecord, self).get_context_data(**kwargs)
        import_record = ImportRecord.objects.get(pk=self.kwargs.get('import_record'))
        context['import_record'] = import_record
        return context


class DeleteQuestion(LoginRequiredMixin, generic.DeleteView):
    '''
    Effacer une question
    '''
    model = Question
    success_url = reverse_lazy('list_question')


class Search(LoginRequiredMixin, generic.TemplateView):
    '''
        Recherche de question
    '''
    template_name = 'qcm/search.html'


class SearchResults(LoginRequiredMixin, generic.ListView):
    model = Question

    def get_queryset(self):
        query = self.request.GET.get('query')
        words = query.split(' ')
        results = Question.objects.all()
        for word in words:
            results = results.filter(text__contains=word)

        return results


# Quiz

class CreateQuiz(LoginRequiredMixin, generic.CreateView):
    model = Quiz

    success_url = reverse_lazy('list_quiz')
    form_class = QuizForm

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(CreateQuiz, self).form_valid(form)


class UpdateQuiz(LoginRequiredMixin, generic.UpdateView):
    model = Quiz
    form_class = QuizForm
    success_url = reverse_lazy('list_quiz')


class ListQuiz(LoginRequiredMixin, generic.ListView):
    model = Quiz

    def get_queryset(self):
        return Quiz.objects.all()


class DeleteQuiz(LoginRequiredMixin, generic.DeleteView):
    model = Quiz
    success_url = reverse_lazy('list_quiz')


class DuplicateQuiz(LoginRequiredMixin, generic.DetailView):
    '''
    Dupliquer une question
    '''
    model = Quiz
    template_name = 'qcm/quiz_duplicate.html'
    context_object_name = 'quiz'

    def post(self, request, pk):
        quiz = Quiz.objects.get(pk=pk)
        quiz.duplicate()
        return redirect(reverse('list_quiz'))


class RandomQuiz(LoginRequiredMixin, generic.TemplateView):
    template_name = 'qcm/quiz_random.html'

    success_url = reverse_lazy('list_quiz')

    def post(self, request, pk):
        n = Quiz.objects.get(pk=pk).questions.count() + 1

        for key in self.request.POST:
            if 'category' in key:
                number_of_questions_to_add = int(self.request.POST[key])
                if number_of_questions_to_add > 0:
                    category_code = key.split('-')[1]
                    category_questions = Question.objects.filter(category__code=category_code)
                    question_ids = list(category_questions.values_list('pk', flat=True))
                    questions_to_add = random.sample(question_ids, number_of_questions_to_add)
                    for question_id in questions_to_add:
                        QuestionOrder.objects.create(quiz_id=pk, question_id=question_id, number=n)
                        n += 1
        return redirect('list_quiz')

    def get_context_data(self, **kwargs):
        '''
        Ajout du quiz et des catégories au contexte
        '''
        context = super(RandomQuiz, self).get_context_data(**kwargs)

        quiz = Quiz.objects.get(pk=self.kwargs['pk'])
        context['quiz'] = quiz

        categories = []
        level_categories = Category.objects.filter(level=quiz.level)

        for category in level_categories:
            categories.append({'level': quiz.level,
                               # todo:vérifiez l'interet de level, aucune utilisation dans la template a priori
                               'category': category,
                               'max_value': Question.objects.filter(category=category).count()
                               })
        context['categories'] = categories
        return context


class TestQuiz(LoginRequiredMixin, generic.TemplateView):
    template_name = 'qcm/quiz_test.html'

    def post(self, request, pk):
        quiz = Quiz.objects.get(pk=pk)

        submission = Submission.objects.create(quiz=quiz, student=request.user.username, test=True)

        for key in request.POST:
            if 'answer' in key:
                question_pk = int(key.split('-')[1])
                value = int(request.POST[key])
                print(value)
                question = Question.objects.get(pk=question_pk)
                # conversion entre le numéro affiché de la réponse et l'ordre enregistré dans la question
                # à revoir pour que le numéro de la template soit celui de question_order pour pouvoir mettre les réponses d'une question dans l'ordre aléatoire

                answer = Answer.objects.create(submission=submission, question=question)
                print(value)
                answer.save_student_answer(value)
        submission.grade(force=True)
        return redirect(reverse_lazy('results_quiz', kwargs={'pk': submission.pk}))

    def get_context_data(self, **kwargs):
        context = super(TestQuiz, self).get_context_data()
        context['quiz'] = Quiz.objects.get(pk=self.kwargs['pk'])
        return context


class AddQuestionQuiz(LoginRequiredMixin, generic.CreateView):
    model = QuestionOrder
    fields = '__all__'


class HtmlQuiz(LoginRequiredMixin, generic.DetailView):
    '''
    Génére une version HTML d'un quiz
    '''

    model = Quiz
    template_name = 'qcm/quiz_html.html'


class LatexQuiz(LoginRequiredMixin, generic.View):
    '''
    Génére le fichier source .tex d'un quiz
    '''

    def get(self, request, pk):
        quiz = Quiz.objects.get(pk=pk)
        latex = quiz.generate_latex()
        response = HttpResponse(latex, content_type='text/plain')
        filename = f'{quiz.title}_sujet'
        response['Content-Disposition'] = f'attachment; filename={filename}.tex'
        return response


class PdfQuiz(LoginRequiredMixin, generic.View):
    '''
    Génére le fichier source .tex d'un quiz et le compile en PDF
    '''

    def get(self, request, pk, option=''):

        quiz = Quiz.objects.get(pk=pk)
        filename = f'{quiz.title}_sujet'
        # TODO: changer le nom du fichier pour éviter des problèms si plusieurs exports sont lancés en même temps
        with open('qcm/templates/latex/latex_export.tex', 'w', encoding='utf-8') as file:
            if option == 'answer':
                file.write(quiz.generate_latex(answer=True))
            else:
                file.write(quiz.generate_latex())
        return render_to_pdf(request, 'latex/latex_export.tex', filename=filename)


class StartQuiz(generic.DetailView):
    object = Quiz
    template_name = 'qcm/quiz_start.html'

    def get_object(self, queryset=None):
        code = int(self.kwargs['code'])
        return Quiz.objects.get(code=code)

    def post(self, request, code):
        code = int(code)
        print('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')

        quiz = Quiz.objects.get(code=code)
        student = request.POST['student']
        student = student.lower().strip()
        submission, created = Submission.objects.get_or_create(quiz=quiz, student=student)
        if created:
            return redirect(reverse_lazy('submit', kwargs={'pk': submission.pk}))
        elif not submission.locked:
            return HttpResponse('continuation du quiz')
        else:
            return HttpResponse('résultats')


class TakeQuiz(generic.DetailView):
    object = Submission
    template_name = 'qcm/quiz_take.html'

    def get_object(self, queryset=None):
        return Submission.objects.get(pk=self.kwargs['pk'])


class ResultsQuiz(LoginRequiredMixin, generic.ListView):
    '''
    Affiche les résultats d'un quiz
    '''
    model = Submission
    template_name = 'qcm/quiz_results.html'

    def get_queryset(self):
        pk = int(self.kwargs['pk'])
        return Submission.objects.filter(quiz_id=pk)

    def get_context_data(self, **kwargs):
        context = super(ResultsQuiz, self).get_context_data(**kwargs)
        pk = int(self.kwargs['pk'])
        context['quiz'] = Quiz.objects.get(pk=pk)

        answers = Answer.objects.filter(submission__quiz__pk=pk)

        query = answers.values('question', 'answer').annotate(c=Count('answer'))
        print(query)
        results = {}
        a = 0
        for element in query:
            print(element['answer'], element['c'])
            question_pk = element['question']

            if question_pk not in results.keys():
                # print(question_pk, results.keys())
                results[question_pk] = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0}

            if element['answer'] is not None:
                results[question_pk][element['answer']] = element['c']
            else:
                print('None', element['c'])
                results[question_pk][5] += element['c']

        context['questions'] = results
        print(results)
        return context
        # todo: reponse par question avec answers.values('question','answer').annotate(dcount=Count('answer'))


@login_required
def import_diu(request):
    dico_theme = {
        '1-Représentation des données : types et valeurs de base': Question.TYPES_DE_BASES,
        '2-Représentation des données : types construits': Question.TYPES_CONSTRUITS,
        '3-Traitement de données en tables': Question.TABLES,
        '4-Interactions entre l’homme et la machine sur le Web': Question.WEB,
        '5-Architectures matérielles et systèmes d’exploitation': Question.ARCHI,
        '6-Langages et programmation': Question.LANG,
        '7-Algorithmique': Question.ALGO}
    print(dico_theme)

    with open('import/lille.csv', encoding='utf-8') as file:
        reader = csv.reader(file, delimiter=';')
        for i in range(3):
            next(reader)
        for line in reader:
            theme = line[0]
            text = line[1]
            answer1 = line[2]
            answer2 = line[3]
            answer3 = line[4]
            answer4 = line[5]
            q = Question(
                text=text,
                answer1=answer1,
                answer2=answer2,
                answer3=answer3,
                answer4=answer4,
                level=PREMIERE,
                category=dico_theme[theme]
            )
            q.save()


################ Submission #################""""

class LockSubmission(generic.DetailView):
    model = Submission
    template_name = 'qcm/submission_confirm_lock.html'

    def post(self, request, pk):
        submission = Submission.objects.get(pk=pk)
        submission.lock()
        if submission.quiz.show_answers == True:
            return redirect(reverse_lazy('results_submission', kwargs={'hash': submission.hash, 'pk': submission.pk}))
        else:
            messages.add_message(request,
                                 messages.SUCCESS,
                                 "Les réponses ont bien été enregistrées.",
                                 extra_tags="Succès")
            return redirect(reverse_lazy('index'))


class ResultsSubmission(generic.DetailView):
    model = Submission
    template_name = 'qcm/submission_results.html'

    def get_object(self, queryset=None):
        hash = str(self.kwargs['hash'])
        pk = int(self.kwargs['pk'])
        # todo: optimiser la requete
        return Submission.objects.get(pk=pk, hash=hash)


################ Ajax ######################

def ajax_answer(request):
    submission_id = request.POST.get('submission_id', None)
    question_id = request.POST.get('question_id', None)
    answer = Answer.objects.get_or_create(submission_id=submission_id, question_id=question_id)
    student_answer = request.POST.get('student_answer', None)
    answer.save_student_answer(student_answer)

    data = {}
    return JsonResponse(data, content_type="application/json", safe=False)


def ajax_test(request):
    print(request.GET)

    submission_id = int(request.GET.get('submission'))
    question_id = int(request.GET.get('question'))
    student_answer = int(request.GET.get('answer'))
    answer = Answer.objects.get(question_id=question_id, submission_id=submission_id)
    answer.save_student_answer(student_answer)
    data = {'answered': True}
    return JsonResponse(data, content_type="application/json", safe=False)


def ajax_test2(request):
    submission_pk = int(request.GET.get('submission'))
    print(submission_pk)
    from django.utils import timezone
    print(timezone.now())
    print(11111111111111111111111111111111111111111111111)
    submission = Submission.objects.get(pk=submission_pk)
    print(2222222222222222222222222222222222222222222)
    questions = submission.quiz.questions.all()
    data = dict()
    data['questions'] = serializers.serialize('json', questions, )

    question_json = list()
    for question in questions:
        question_json.append({'pk': question.pk,
                              'text': question.text_html,
                              'answer1': question.get_answer_html_for_frontend(1),
                              'answer2': question.get_answer_html_for_frontend(2),
                              'answer3': question.get_answer_html_for_frontend(3),
                              'answer4': question.get_answer_html_for_frontend(4),
                              })
    data['questions'] = json.dumps(question_json)
    return JsonResponse(data, content_type="application/json", safe=False)


class Vue(generic.TemplateView):
    template_name = 'qcm/quiz_take.html'


def ajax_quiz(request):
    quiz_pk = int(request.GET.get('quiz'))
    quiz = Quiz.objects.get(pk=quiz_pk)

    questions = quiz.questions.all()
    data = dict()
    data['questions'] = serializers.serialize('json', questions, )
    # todo : récrire la requete à partir de question order
    question_json = list()
    for question in questions:
        questionorder = QuestionOrder.objects.get(quiz=quiz, question=question)
        question_json.append({'pk': question.pk,
                              'text': question.text_html,
                              'questionorder': questionorder.pk,
                              'first_line': question.first_line(100),
                              'answer1': question.get_answer_html_for_frontend(1),
                              'answer2': question.get_answer_html_for_frontend(2),
                              'answer3': question.get_answer_html_for_frontend(3),
                              'answer4': question.get_answer_html_for_frontend(4),
                              })
    data['questions'] = json.dumps(question_json)
    return JsonResponse(data, content_type="application/json", safe=False)


def ajax_questionorder_delete(request):
    '''
    enlève la question du quiz
    '''
    questionorder_pk = int(request.GET.get('questionorder'))
    QuestionOrder.objects.get(pk=questionorder_pk).delete()
    data = []
    return JsonResponse(data, content_type="application/json", safe=False)
