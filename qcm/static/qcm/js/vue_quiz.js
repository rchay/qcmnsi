Vue.component('question', {
    props: ['quiz', 'question',],
    data: function() {
        return {
            visible: true,
            details: false,
        }
    },
    methods: {
        deleteQuestion: function() {
            axios
                .get('/ajax/questionorder/delete/', {
                    params: {
                        'questionorder': this.question.questionorder,
                    }
                })
                .then(response => (this.visible = false))
        },
    },

    template: `<tr v-if=visible>
    <td>
        <span v-html=question.first_line></span>
    </td>
    <td>
        <span class="icon has-text-info" title="Voir la question"><i class="far fa-eye" aria-hidden="true"
                                                                     @click="details=true"></i></span>
    </td>
    <td>
            <span class="icon has-text-danger" title="Enlever la question">
            <i class="fas fa-times" aria-hidden="true" v-on:click="deleteQuestion"></i>
            </span>
    </td>

    <div class="modal" v-bind:class="{'is-active': details}">
        <div class="modal-background" @click="details=true"></div>
        <div class="modal-content">
            <div class="modal-card">
                <section class="modal-card-body">
                    <div class="content">
                        <div v-html=question.text></div>
                    </div>
                    <div class="content">
                        <div class="field">
                            <div class="input is-primary" style="height: auto;">
                                <div class="content" style="width: 100%;" v-html=question.answer1>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="input is-danger" style="height: auto;">
                                <div class="content" style="width: 100%;" v-html=question.answer2></div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="input is-danger" style="height: auto;">
                                <div class="content" style="width: 100%;" v-html=question.answer3></div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="input is-danger" style="height: auto;">
                                <div class="content" style="width: 100%;" v-html=question.answer4></div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <button class="modal-close is-large" aria-label="close" @click="details=false"></button>
    </div>

</tr>`
})


var app = new Vue({
    el: '#app',
    delimiters: ['[[', ']]'],
    data: {
        questions: null,
    },
    methods: {
        init() {
            quiz = document.getElementById('app').dataset.quiz
            axios
                .get('/ajax/quiz/', {
                    params: {
                        'quiz': quiz
                    }
                })
                .then(response => (this.questions = JSON.parse(response.data.questions)))
                .then(function () {MathJax.typesetPromise()})
            },
    },
    mounted: function() {
        this.init()
    }
})


