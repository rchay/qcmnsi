document.addEventListener('DOMContentLoaded', () => {
  (document.querySelectorAll('.delete') || []).forEach(($delete) => {
    $notification = $delete.parentNode.parentNode;
    $delete.addEventListener('click', () => {
      $notification.parentNode.removeChild($notification);
    });
  });
});