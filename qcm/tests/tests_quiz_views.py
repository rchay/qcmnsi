from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from structure.models import Level, Category
from qcm.models import Quiz


class QuizCreateViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_superuser(email='admin@example.com', username='admin', password='motdepasse')
        User.objects.create_user(email='prof@example.com', username='prof', password='motdepasse')
        Level.objects.create(pk=1, name='Premiere')
        Category.objects.create(pk=11, code='11', name='Types de bases', level_id=1)

    def test_call_view_denies_anonymous(self):
        response = self.client.get('/quiz/create/')
        self.assertRedirects(response, '/login/?next=/quiz/create/', status_code=302, target_status_code=200)
        response = self.client.get(reverse('create_quiz'))
        self.assertRedirects(response, '/login/?next=/quiz/create/', status_code=302, target_status_code=200)

    def test_call_view_loads(self):
        self.client.login(username='prof', password='motdepasse')
        response = self.client.get(reverse('create_quiz'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'qcm/quiz_form.html')

    def test_call_view_fails_blank(self):
        self.client.login(username='prof', password='motdepasse')
        response = self.client.post(reverse('create_quiz'), {})
        self.assertFormError(response, 'form', 'title', 'Ce champ est obligatoire.')

    def test_call_view_success(self):
        self.client.login(username='prof', password='motdepasse')

        new_quiz = {'title': 'python',
                    'level': '1',
                    'points_right_answer': '3',
                    'points_wrong_answer': '-1',
                    }
        response = self.client.post(reverse('create_quiz'), new_quiz)

        self.assertEqual(Quiz.objects.all().count(), 1)
        self.assertRedirects(response, reverse('list_quiz'), status_code=302, target_status_code=200)
