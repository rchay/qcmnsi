from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from qcm.models import Question, Level, Category


class QuestionCreateViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_superuser(email='admin@example.com', username='admin', password='motdepasse')
        User.objects.create_user(email='prof@example.com', username='prof', password='motdepasse')
        Level.objects.create(pk=1, name='Premiere')
        Category.objects.create(pk=11, code='11', name='Types de bases', level_id=1)

    def test_call_view_denies_anonymous(self):
        response = self.client.get('/question/create/')
        self.assertRedirects(response, '/login/?next=/question/create/', status_code=302, target_status_code=200)
        response = self.client.get(reverse('create_question'))
        self.assertRedirects(response, '/login/?next=/question/create/', status_code=302, target_status_code=200)

    def test_call_view_loads(self):
        self.client.login(username='prof', password='motdepasse')
        response = self.client.get(reverse('create_question'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'qcm/question_form.html')

    def test_call_view_fails_blank(self):
        self.client.login(username='prof', password='motdepasse')
        response = self.client.post(reverse('create_question'), {})
        self.assertFormError(response, 'form', 'level', 'Ce champ est obligatoire.')
        self.assertFormError(response, 'form', 'category', 'Ce champ est obligatoire.')
        self.assertFormError(response, 'form', 'text', 'Ce champ est obligatoire.')
        self.assertFormError(response, 'form', 'answer1', 'Ce champ est obligatoire.')
        self.assertFormError(response, 'form', 'answer2', 'Ce champ est obligatoire.')
        self.assertFormError(response, 'form', 'answer3', 'Ce champ est obligatoire.')
        self.assertFormError(response, 'form', 'answer4', 'Ce champ est obligatoire.')
        self.assertFormError(response, 'form', 'author', 'Ce champ est obligatoire.')
        self.assertFormError(response, 'form', 'tags', 'Ce champ est obligatoire.')


    def test_call_view_success(self):
        self.client.login(username='prof', password='motdepasse')

        new_question = {'level': '1', 'category': '11', 'text': 'énoncé',
                        'answer1': 'réponse correcte', 'answer2': 'réponse incorrecte',
                        'answer3': 'réponse correcte', 'answer4': 'réponse incorrecte',
                        'author': 'prénom nom', 'tags': 'tag1', 'next':''
                        }
        response = self.client.post(reverse('create_question'), new_question)

        self.assertEqual(Question.objects.all().count(), 1)
        #todo: tester la redirection une fois qu'elle est fixée

