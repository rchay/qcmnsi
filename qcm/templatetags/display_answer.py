from django import template
register = template.Library()

@register.filter(name='display_answer_html')
def display_answer_html(question, number):
    if number == 1:
        return question.answer1_html
    elif number == 2:
        return question.answer2_html
    elif number == 3:
        return question.answer3_html
    elif number == 4:
        return question.answer4_html


@register.filter(name='display_answer_latex')
def display_answer_latex(question, number):
    if number == 1:
        return '\CorrectChoice ' + question.answer1_latex
    elif number == 2:
        return '\choice ' + question.answer2_latex
    elif number == 3:
        return '\choice ' + question.answer3_latex
    elif number == 4:
        return '\choice ' + question.answer4_latex
