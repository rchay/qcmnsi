import itertools
import json
import random
import re
import secrets

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers import PythonLexer, JavascriptLexer, SqlLexer
from pygments.lexers.shell import PowerShellLexer
from pygments.lexers.html import HtmlLexer
from taggit.managers import TaggableManager
from .constants import *

from structure.models import Level, Category
from importexport.models import ImportRecord


def convert_html(text):
    text = text.replace('<latex>', '$$$')
    text = text.replace('</latex>', '$$$')

    # mettre en forme le code compris entre les balises <python> avec la librairie pygments
    for block_tags in re.findall('(<python>([\w\W]+?)</python>)', text):
        text = text.replace(block_tags[0], highlight(block_tags[1], PythonLexer(), HtmlFormatter()))
    for block_tags in re.findall('(<js>([\w\W]+?)</js>)', text):
        text = text.replace(block_tags[0], highlight(block_tags[1], JavascriptLexer(), HtmlFormatter()))
    for block_tags in re.findall('(<sql>([\w\W]+?)</sql>)', text):
        text = text.replace(block_tags[0], highlight(block_tags[1], SqlLexer(), HtmlFormatter()))
    for block_tags in re.findall('(<shell>([\w\W]+?)</shell>)', text):
        text = text.replace(block_tags[0], highlight(block_tags[1], PowerShellLexer(), HtmlFormatter()))
    for block_tags in re.findall('(<html>([\w\W]+?)</html>)', text):
        text = text.replace(block_tags[0], highlight(block_tags[1], HtmlLexer(), HtmlFormatter()))
    for block_tags in re.findall('(<esc>([\w\W]+?)</esc>)', text):
        text = text.replace(block_tags[0], block_tags[1].replace('<', '&lt').replace('>', '&gt'))

    return text


def convert_latex(text):
    text = text.replace('&nbsp;', ' ')
    text = text.replace('&lt;', '<')
    text = text.replace('&gt;', '>')
    text = text.replace('%', '$\%$')
    text = text.replace('#', '$\#$')
    text = text.replace('_', '$\_$')
    text = text.replace('\r\n', '\n')  # copier coller depuis pycharm
    # text = text.replace('{', '$\{$')
    # text = text.replace('}', '$\}$')
    text = text.replace('$\{$$\}$', '$\{\}$')  # resultat si dict vide {}
    text = text.replace('<latex>', '$ ')
    text = text.replace('</latex>', ' $')

    # text = text.replace('_', '\_')
    # text = text.replace('#', '\#')
    # mettre en forme le code compris entre les balises <python> avec le module listings
    for block_tags in re.findall('(<python>([\w\W]+?)</python>)', text):
        text = text.replace(block_tags[0], '\\begin{lstlisting}[style=python]' + block_tags[1] + '\\end{lstlisting}')
    # mettre en forme le code compris entre les balises <js> avec le module listings
    for block_tags in re.findall('(<js>([\w\W]+?)</js>)', text):
        text = text.replace(block_tags[0],
                            '\\begin{lstlisting}[style=javascript]' + block_tags[1] + '\\end{lstlisting}')
    # mettre en forme le code en ligne compris entre les balises <tt> avec l'instruction \texttt
    for block_tags in re.findall('(<tt>([\w\W]+?)</tt>)', text):
        text = text.replace(block_tags[0], '\\lstinline[mathescape, style=codestyle]{' + block_tags[1] + '}')

    return text


class Question(models.Model):
    '''
    Question à choix multiples avec une seule réponse correcte
    '''

    # Ordre des questions choisi parmi toutes les permutations de [1, 2, 3, 4]
    ORDERS = [json.dumps(permutation) for permutation in list(itertools.permutations([1, 2, 3, 4]))]

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # todo: enlever level du modele de question car il est accessible par category, pourrait poser problème si on
    # change le niveau d'une catégorie (à revoir avec le chargement dynamique des catégorie en fonction du niveau
    level = models.ForeignKey(Level, on_delete=models.SET_NULL, null=True, blank=False, default=1)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=False, default=11)
    created = models.DateTimeField('Création', auto_now_add=True)
    modified = models.DateTimeField('Dernière modification', auto_now=True)
    text = models.TextField('Enoncé', max_length=2000)
    text_html = models.TextField(max_length=2000, default='')
    text_latex = models.TextField(max_length=2000, default='')
    import_record = models.ForeignKey(ImportRecord, on_delete=models.SET_NULL, null=True)
    tags = TaggableManager('Mots-clés')
    answer_order_json = models.CharField('Ordre des réponses', max_length=32)
    author = models.CharField('Auteur', max_length=128, default='')

    answer1 = models.TextField('Réponse correcte', max_length=2000, )
    answer2 = models.TextField('Réponse incorrecte', max_length=2000)
    answer3 = models.TextField('Réponse incorrecte', max_length=2000)
    answer4 = models.TextField('Réponse incorrecte', max_length=2000)

    answer1_html = models.TextField(max_length=2000, default='')
    answer2_html = models.TextField(max_length=2000, default='')
    answer3_html = models.TextField(max_length=2000, default='')
    answer4_html = models.TextField(max_length=2000, default='')

    answer1_latex = models.TextField(max_length=2000, default='')
    answer2_latex = models.TextField(max_length=2000, default='')
    answer3_latex = models.TextField(max_length=2000, default='')
    answer4_latex = models.TextField(max_length=2000, default='')

    @property
    def answer_order(self):
        return json.loads(self.answer_order_json)

    def first_line(self, limit=None):
        ''' renvoie la premiere ligne de l'énoncé pour la vue compacte d'une question'''
        first_line = self.text_html.split('\n')[0]
        if limit is None:
            return first_line
        elif len(first_line) > limit:
            return first_line[:limit] + '...'
        else:
            return first_line

    def get_answer_html_for_frontend(self, i):
        i = i - 1
        if self.answer_order[i] == 1:
            return self.answer1_html
        elif self.answer_order[i] == 2:
            return self.answer2_html
        elif self.answer_order[i] == 3:
            return self.answer3_html
        elif self.answer_order[i] == 4:
            return self.answer4_html

    def save(self, *args, **kwargs):
        # création de la question

        if self.pk is None:
            # ordre des questions choisi parmi les permutations de [1, 2, 3, 4]
            self.answer_order_json = random.choice(self.ORDERS)

        # générer les versions html des champs
        self.text_html = convert_html(self.text)
        self.answer1_html = convert_html(self.answer1)
        self.answer2_html = convert_html(self.answer2)
        self.answer3_html = convert_html(self.answer3)
        self.answer4_html = convert_html(self.answer4)

        # générer les versions LaTeX des champs
        self.text_latex = convert_latex(self.text)
        self.answer1_latex = convert_latex(self.answer1)
        self.answer2_latex = convert_latex(self.answer2)
        self.answer3_latex = convert_latex(self.answer3)
        self.answer4_latex = convert_latex(self.answer4)

        super(Question, self).save(*args, **kwargs)


class Quiz(models.Model):
    '''
    Ensemble de Questions
    '''
    title = models.CharField('Titre ', max_length=200)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    level = models.ForeignKey(Level, on_delete=models.SET_NULL, null=True, blank=False, default=1)
    created = models.DateTimeField('Création', auto_now_add=True)
    modified = models.DateTimeField('Dernière modification', auto_now=True)
    code = models.PositiveIntegerField('Code ', unique=True)
    questions = models.ManyToManyField(Question, related_name='in_quiz', through='QuestionOrder')
    points_right_answer = models.SmallIntegerField('Points pour une réponse correcte', default=3)
    points_wrong_answer = models.SmallIntegerField('Points pour une réponse fausse', default=-1)

    random_question_order = models.BooleanField('Ordre aléatoire', default=False)
    show_answers = models.BooleanField('Montrer la solution', default=False)

    class Meta:
        ordering = ['-modified']

    @property
    def number_of_questions(self):
        return len(self.questions.all())

    @property
    def points_total(self):
        return self.number_of_questions * self.points_right_answer

    @property
    def stats(self):
        locked_submissions = self.submission_set.exclude(locked=False)
        stats = dict(locked_submissions.aggregate(
            right=models.Avg('count_right') / self.number_of_questions * 100,
            wrong=models.Avg('count_wrong') / self.number_of_questions * 100,
            no_answer=models.Avg('count_no_answer') / self.number_of_questions * 100,
            average=models.Avg('points_scored')),
            locked_submissions=locked_submissions.count())

        return stats

    def authors(self):
        authors = set()
        for question in self.questions.all():
            if question.author != '':
                authors.add(question.author)
        return list(authors)

    def authors_display(self):
        authors = self.authors()
        if len(authors) == 1:
            return authors[0]
        elif len(authors) == 2:
            return authors[0] + ' et ' + authors[1]
        else:
            return ', '.join(authors) + ' et ' + authors[-1]

    def license(self):
        text = 'Ce document est basé sur les travaux de '
        text += self.authors_display()
        text += ''' et est mis à disposition selon les termes de la Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0'''
        return text

    def generate_latex(self, answer=False):
        # todo: déporter generate latex avecnles fonctions html et latex pour pouvoir reutiliser ca pour l'export de la base
        header = LATEX_HEADER

        if answer:
            header += '\\printanswers\n'

        body = '\\begin{document} '
        body += '\\begin {framed}\n \\begin{tabular}{p{13.5cm}p{3cm}}\n'
        body += self.license()
        body += '&\n \\raisebox{-.5\height}{'
        body += LATEX_BYNCSA
        body += '''}\ \\\\
        \end{tabular}
        \end{framed}
        '''
        body += '\\begin{questions} \n'
        for question in self.questions.all():
            body += '\question ' + question.text_latex + '\n\n'
            body += '\\begin{checkboxes}\n'
            for n in question.answer_order:
                if n == 1:
                    body += '\\CorrectChoice ' + question.answer1_latex + '\n'
                elif n == 2:
                    body += '\\choice ' + question.answer2_latex + '\n'
                elif n == 3:
                    body += '\\choice ' + question.answer3_latex + '\n'
                elif n == 4:
                    body += '\\choice ' + question.answer4_latex + '\n'
            body += '\\end{checkboxes}\n\n'
        footer = ''' \\end{questions} \\end{document}'''
        return header + body + footer

    def duplicate(self):
        new_quiz = Quiz(title=self.title + ' (copie)',
                        user=self.user,
                        level=self.level,
                        points_right_answer=self.points_right_answer,
                        points_wrong_answer=self.points_wrong_answer,
                        random_question_order=self.random_question_order,
                        )
        new_quiz.save()
        for qo in self.questionorder_set.all():
            QuestionOrder.objects.create(quiz_id=new_quiz.pk, question_id=qo.question.pk, number=qo.number)

    def save(self, *args, **kwargs):
        # création du quiz
        if self.pk is None:
            # générer un numéro de quiz
            all_codes = list(Quiz.objects.values_list('code', flat=True))
            while True:
                new_code = random.randrange(100000, 1000000)
                if new_code not in all_codes:
                    self.code = new_code
                    break
        super(Quiz, self).save(*args, **kwargs)


class QuestionOrder(models.Model):
    '''
    Ordre des questions dans un quiz
    '''
    number = models.PositiveSmallIntegerField('Numero')
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)


class Submission(models.Model):
    '''
    Ensembles des réponses d'une personne à un quiz
    '''
    student = models.CharField('Nom et prénom', max_length=200)
    quiz = models.ForeignKey('Quiz', on_delete=models.CASCADE)
    started = models.DateTimeField('Début', auto_now_add=True)
    finished = models.DateTimeField('Fin', default=None, null=True)
    test = models.BooleanField('Test Prof', default=False)
    locked = models.BooleanField('Vérouillé', default=False)
    hash = models.CharField(default=secrets.token_hex, max_length=64)
    points_scored = models.SmallIntegerField('Points marqués', default=None, null=True)
    count_right = models.PositiveSmallIntegerField('Réponses correctes', default=None, null=True)
    count_wrong = models.PositiveSmallIntegerField('Réponses fausses', default=None, null=True)
    count_no_answer = models.PositiveSmallIntegerField('Pas de réponses', default=None, null=True)

    class Meta:
        ordering = ['student']

    def save(self, *args, **kwargs):
        if self.pk is None:
            super(Submission, self).save(*args, **kwargs)
            self.initialize_answers()
        else:
            super(Submission, self).save(*args, **kwargs)

    def initialize_answers(self):
        '''
        Création d'objets Answer pour toutes les questions du quiz
        :return:
        '''
        for question in self.quiz.questions.all():
            Answer.objects.create(submission=self, question=question)

    def lock(self):
        '''
        Verouillage et déclenchement du calcul des points
        '''
        self.locked = True
        self.finished = timezone.now()
        self.save()
        self.grade()

    def grade(self, force=False):
        '''
        Calcul des points
        '''
        points_scored = 0
        points_right_awswer = self.quiz.points_right_answer
        points_wrong_awswer = self.quiz.points_wrong_answer
        for answer in self.answer_set.all():
            answer.grade(points_right_answer=points_right_awswer, points_wrong_awswer=points_wrong_awswer, force=force)
            points_scored += answer.points_scored
        self.points_scored = points_scored
        self.count_right = self.answer_set.filter(status=Answer.RIGHT).count()
        self.count_wrong = self.answer_set.filter(status=Answer.WRONG).count()
        self.count_no_answer = self.answer_set.filter(status=Answer.UNANSWERED).count()
        self.save(update_fields=['points_scored', 'count_right', 'count_wrong', 'count_no_answer'])


class Answer(models.Model):
    '''
    Réponse d'une personne à une question pour un quiz donné
    '''
    ANSWER1, ANSWER2, ANSWER3, ANSWER4, NO_ANSWER = 1, 2, 3, 4, 5
    ANSWERS = (
        (ANSWER1, 'Réponse correcte'),
        (ANSWER2, 'Réponse incorrecte'),
        (ANSWER3, 'Réponse incorrecte'),
        (ANSWER4, 'Réponse incorrecte'),
        (NO_ANSWER, 'Non répondu'),
    )

    BLANK, UNGRADED, UNANSWERED, WRONG, RIGHT = 0, 1, 2, 3, 4
    STATUS_CHOICES = (
        (BLANK, 'non répondu'),
        (UNGRADED, 'non noté'),
        (UNANSWERED, 'sans réponse'),
        (WRONG, 'réponse fausse'),
        (RIGHT, 'réponse correcte'),
    )

    submission = models.ForeignKey('Submission', on_delete=models.CASCADE)
    status = models.PositiveSmallIntegerField('Status', choices=STATUS_CHOICES, default=UNGRADED)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    answer = models.PositiveSmallIntegerField('Réponse', choices=ANSWERS, null=True)
    created = models.DateTimeField('Création', auto_now_add=True)
    modified = models.DateTimeField('Dernière modification', auto_now=True)
    points_scored = models.SmallIntegerField('Points marqués', default=None, null=True)

    def save_student_answer(self, student_answer):
        '''
        Convertit le numéro de réponse
        '''
        if self.submission.locked:
            pass
        else:
            if student_answer != self.NO_ANSWER:
                # soustraire 1 pour obtenir l'élément correct de la liste
                self.answer = self.question.answer_order[student_answer - 1]
            else:
                self.answer = self.NO_ANSWER
            self.status = self.UNGRADED
            self.save(update_fields=["answer", "status"])

    def grade(self, points_right_answer, points_wrong_awswer, force=False):
        '''
        Détermine et enregistre le nombre de points marqués
        '''

        if self.status == self.UNGRADED or self.status == self.BLANK or force:
            if self.answer in [self.ANSWER2, self.ANSWER3, self.ANSWER4]:
                self.points_scored = points_wrong_awswer
                self.status = self.WRONG
            elif self.answer == self.ANSWER1:
                self.points_scored = points_right_answer
                self.status = self.RIGHT
            else:
                self.points_scored = 0
                self.status = self.UNANSWERED
                self.answer = self.NO_ANSWER
            self.save(update_fields=['points_scored', 'status', 'answer'])
        else:
            pass



