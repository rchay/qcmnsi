from django.urls import path, reverse_lazy, include
from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('question/', views.ListQuestions.as_view(), name='list_question'),
    path('question/tag/<str:tag>/', views.ListQuestionsByTag.as_view(), name='list_question_by_tag'),
    path('question/category/<int:category>/', views.ListQuestionsByCategory.as_view(),
         name='list_question_by_category'),
    path('question/import/<int:import_record>/', views.ListQuestionsByImportRecord.as_view(),
         name='list_question_by_import_record'),
    path('question/create/', views.CreateQuestionView.as_view(),
         name='create_question'),
    path('question/update/<int:pk>/', views.UpdateQuestion.as_view(), name='update_question'),
    path('question/delete/<int:pk>/', views.DeleteQuestion.as_view(), name='delete_question'),

    path('search/', views.Search.as_view(), name='search'),
    path('search/results/', views.SearchResults.as_view(), name='search_results'),
]

urlpatterns += [
    path('quiz/', views.ListQuiz.as_view(), name='list_quiz'),
    path('quiz/create/', views.CreateQuiz.as_view(success_url=reverse_lazy('list_quiz')), name='create_quiz'),
    path('quiz/<int:pk>/update/', views.UpdateQuiz.as_view(), name='update_quiz'),
    path('quiz/<int:pk>/delete/', views.DeleteQuiz.as_view(), name='delete_quiz'),
    path('quiz/<int:pk>/duplicate/', views.DuplicateQuiz.as_view(), name='duplicate_quiz'),
    path('quiz/<int:pk>/random/', views.RandomQuiz.as_view(), name='random_quiz'),
    path('quiz/<int:pk>/test/', views.TestQuiz.as_view(), name='test_quiz'),
    path('quiz/<int:pk>/results/', views.ResultsQuiz.as_view(), name='results_quiz'),

    path('quiz/<int:pk>/html/', views.HtmlQuiz.as_view(), name='html_quiz'),
    path('quiz/<int:pk>/latex/', views.LatexQuiz.as_view(), name='latex_quiz'),
    path('quiz/<int:pk>/pdf/', views.PdfQuiz.as_view(), name='pdf_quiz'),
    path('quiz/<int:pk>/pdf/<str:option>/', views.PdfQuiz.as_view(), name='pdf_quiz_with_option'),
    path('quiz/<int:pk>/add/', views.AddQuestionQuiz.as_view(success_url=reverse_lazy('list_quiz')),
         name='add_question_quiz'),
]

urlpatterns += [
    path('quiz/start/<int:code>/', views.StartQuiz.as_view(), name='start_quiz'),

    # path('quiz/results/<int:pk>/', views.SubmissionResults.as_view(), name='results_quiz'),
    path('submission/<int:pk>/', views.TakeQuiz.as_view(), name='submit'),
    path('submission/<int:pk>/lock/', views.LockSubmission.as_view(), name='lock_submission'),
    path('submission/<int:pk>/result/<str:hash>/', views.ResultsSubmission.as_view(), name='results_submission'),
]

urlpatterns += [
    path('vue/', views.Vue.as_view(), name='vue'),
    path('ajax/', views.ajax_test, name='ajax'),
    path('ajax2/', views.ajax_test2, name='ajax2'),
    path('ajax/quiz/', views.ajax_quiz, name='ajax_quiz'),
    path('ajax/questionorder/delete/', views.ajax_questionorder_delete, name='ajax_questionorder_delete'),

]
