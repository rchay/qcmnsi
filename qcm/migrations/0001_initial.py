# Generated by Django 2.2.7 on 2020-03-22 12:31

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import taggit.managers


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('structure', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('taggit', '0003_taggeditem_add_unique_index'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImportRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('file_format', models.PositiveSmallIntegerField(choices=[(1, '.CSV'), (2, '.GIFT'), (3, '.SQL'), (4, '.MD'), (5, '.TEX')], verbose_name='Format')),
                ('file_name', models.CharField(max_length=512, verbose_name='Nom de fichier')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Création')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Dernière modification')),
                ('text', models.TextField(max_length=2000, verbose_name='Enoncé')),
                ('text_html', models.TextField(default='', max_length=2000)),
                ('text_latex', models.TextField(default='', max_length=2000)),
                ('answer_order_json', models.CharField(max_length=32, verbose_name='Ordre des réponses')),
                ('author', models.CharField(default='', max_length=128, verbose_name='Auteur')),
                ('answer1', models.TextField(max_length=2000, verbose_name='Réponse correcte')),
                ('answer2', models.TextField(max_length=2000, verbose_name='Réponse incorrecte')),
                ('answer3', models.TextField(max_length=2000, verbose_name='Réponse incorrecte')),
                ('answer4', models.TextField(max_length=2000, verbose_name='Réponse incorrecte')),
                ('answer1_html', models.TextField(default='', max_length=2000)),
                ('answer2_html', models.TextField(default='', max_length=2000)),
                ('answer3_html', models.TextField(default='', max_length=2000)),
                ('answer4_html', models.TextField(default='', max_length=2000)),
                ('answer1_latex', models.TextField(default='', max_length=2000)),
                ('answer2_latex', models.TextField(default='', max_length=2000)),
                ('answer3_latex', models.TextField(default='', max_length=2000)),
                ('answer4_latex', models.TextField(default='', max_length=2000)),
                ('category', models.ForeignKey(default=11, null=True, on_delete=django.db.models.deletion.SET_NULL, to='structure.Category')),
                ('import_record', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='qcm.ImportRecord')),
                ('level', models.ForeignKey(default=1, null=True, on_delete=django.db.models.deletion.SET_NULL, to='structure.Level')),
                ('tags', taggit.managers.TaggableManager(help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Mots-clés')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='QuestionOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.PositiveSmallIntegerField(verbose_name='Numero')),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='qcm.Question')),
            ],
        ),
        migrations.CreateModel(
            name='Quiz',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='Titre ')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Création')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Dernière modification')),
                ('code', models.PositiveIntegerField(unique=True, verbose_name='Code ')),
                ('points_right_answer', models.SmallIntegerField(default=3, verbose_name='Points pour une réponse correcte')),
                ('points_wrong_answer', models.SmallIntegerField(default=-1, verbose_name='Points pour une réponse fausse')),
                ('random_question_order', models.BooleanField(default=False, verbose_name='Ordre aléatoire')),
                ('level', models.ForeignKey(default=1, null=True, on_delete=django.db.models.deletion.SET_NULL, to='structure.Level')),
                ('questions', models.ManyToManyField(related_name='in_quiz', through='qcm.QuestionOrder', to='qcm.Question')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-modified'],
            },
        ),
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('student', models.CharField(max_length=200, verbose_name='Nom et prénom')),
                ('started', models.DateTimeField(auto_now_add=True, verbose_name='Début')),
                ('finished', models.DateTimeField(default=None, null=True, verbose_name='Fin')),
                ('test', models.BooleanField(default=False, verbose_name='Test Prof')),
                ('locked', models.BooleanField(default=False, verbose_name='Vérouillé')),
                ('points_scored', models.SmallIntegerField(default=None, null=True, verbose_name='Points marqués')),
                ('count_right', models.PositiveSmallIntegerField(default=None, null=True, verbose_name='Réponses correctes')),
                ('count_wrong', models.PositiveSmallIntegerField(default=None, null=True, verbose_name='Réponses fausses')),
                ('count_no_answer', models.PositiveSmallIntegerField(default=None, null=True, verbose_name='Pas de réponses')),
                ('quiz', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='qcm.Quiz')),
            ],
            options={
                'ordering': ['student'],
            },
        ),
        migrations.AddField(
            model_name='questionorder',
            name='quiz',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='qcm.Quiz'),
        ),
        migrations.CreateModel(
            name='ExportRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('format', models.PositiveSmallIntegerField(choices=[(1, '.CSV'), (2, '.GIFT'), (3, '.SQL'), (4, '.MD'), (5, '.TEX')], verbose_name='Format')),
                ('filepath', models.CharField(max_length=200, null=True, verbose_name='Titre ')),
                ('question_number', models.PositiveSmallIntegerField(null=True, verbose_name='Nombre de questions')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='Answer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.PositiveSmallIntegerField(choices=[(0, 'non répondu'), (1, 'non noté'), (2, 'sans réponse'), (3, 'réponse fausse'), (4, 'réponse correcte')], default=1, verbose_name='Status')),
                ('answer', models.PositiveSmallIntegerField(choices=[(1, 'Réponse correcte'), (2, 'Réponse incorrecte'), (3, 'Réponse incorrecte'), (4, 'Réponse incorrecte'), (5, 'Non répondu')], null=True, verbose_name='Réponse')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Création')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Dernière modification')),
                ('points_scored', models.SmallIntegerField(default=None, null=True, verbose_name='Points marqués')),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='qcm.Question')),
                ('submission', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='qcm.Submission')),
            ],
        ),
    ]
