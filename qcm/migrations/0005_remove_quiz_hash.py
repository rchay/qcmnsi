# Generated by Django 3.0.5 on 2020-04-11 21:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('qcm', '0004_quiz_hash'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='quiz',
            name='hash',
        ),
    ]
