from django import forms
from .models import Question, Quiz, Category


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['level', 'category', 'tags', 'author', 'text', 'answer1', 'answer2', 'answer3', 'answer4']
        widgets = {
            'text': forms.Textarea(attrs={'class': 'textarea',
                                          'placeholder': "Entrer l'énoncé de la question",
                                          'rows': 8}),
            'answer1': forms.Textarea(attrs={'class': 'textarea',
                                             'placeholder': 'Entrer la réponse correcte.',
                                             'rows': 2},
                                      ),
            'answer2': forms.Textarea(attrs={'class': 'textarea',
                                             'placeholder': 'Entrer une première réponse incorrecte.',
                                             'rows': 2}),
            'answer3': forms.Textarea(attrs={'class': 'textarea',
                                             'placeholder': 'Entrer une deuxième réponse incorrecte.',
                                             'rows': 2}),
            'answer4': forms.Textarea(attrs={'class': 'textarea',
                                             'placeholder': 'Entrer une troisième réponse incorrecte.',
                                             'rows': 2}),
            # 'tags': forms.TextInput(attrs={'class': 'input',
            #                               #'type': 'text'
            #                               }
            #                       )

        }

    def __init__(self, *args, **kwargs):
        ''' Chargement des catégories en fonction de la classe'''
        super(QuestionForm, self).__init__(*args, **kwargs)
        self.fields['category'].queryset = Category.objects.filter(level_id=1)



class QuizForm(forms.ModelForm):
    class Meta:
        model = Quiz
        fields = ['title', 'level', 'points_right_answer', 'points_wrong_answer', 'random_question_order', 'show_answers']
        widgets = {'title': forms.TextInput(attrs={'class': 'input', 'type': 'text'}),
                   'level': forms.Select(attrs={'class': 'select'}),
                   'points_right_answer': forms.NumberInput(attrs={'class': 'input', 'min': '1'}),
                   'points_wrong_answer': forms.NumberInput(attrs={'class': 'input', 'max': '0'}),
                   }
