from .constants import *


def category_codes(request):
    # return the value you want as a dictionnary. you may add multiple values in there.
    return {
        'TYPES_DE_BASES': TYPES_DE_BASES,
        'TYPES_CONSTRUITS': TYPES_CONSTRUITS,
        'TABLES': TABLES,
        'WEB': WEB,
        'ARCHI_1': ARCHI_1,
        'LANG_1': LANG_1,
        'ALGO_1': ALGO_1
    }
