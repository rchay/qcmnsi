from django.core.management.base import BaseCommand, CommandError

from django.contrib.auth.models import User
from django.db import IntegrityError
import getpass


class Command(BaseCommand):
    help = 'Ajoute un nouveau professeur'

    def handle(self, *args, **options):
        username = input("nom d'utilisateur ? ")
        email = input('email ? ')
        first_name = input('prénom ? ')
        last_name = input('nom de famille ? ')
        password = getpass.getpass('mot de passe ? ')
        # todo: conditions pour que les valeurs ne soient pas nulles
        try:
            User.objects.create_user(username=username,
                                      email=email,
                                      password=password,
                                      last_name=last_name,
                                      first_name=first_name).save()
            self.stdout.write(self.style.SUCCESS(f"Création du nouvel professeur {username}"))
        except IntegrityError:
            self.stdout.write(self.style.NOTICE("Le nom d'utilisateur est déja pris"))
        except ValueError as e:
            if str(e) == 'The given username must be set':
                self.stdout.write(self.style.NOTICE("Le nom d'utilisateur ne peut pas être vide."))
            else:
                print(str(e))


