from django.core.management.base import BaseCommand, CommandError

import csv
from structure.models import Level, Category
from django.contrib.auth.models import User


class Command(BaseCommand):
    help = 'Remplit la base de données après une remise à zéro'

    def handle(self, *args, **options):
        if not (User.objects.first()):
            User.objects.create_superuser(username="admin",
                                          email="admin@gmail.com",
                                          password="motdepasse",
                                          last_name="Administrateur").save()

        print('Remplissage de la base de données')
        Level.objects.all().delete()
        Category.objects.all().delete()

        with open('qcm/management/commands/levels.csv', 'r', encoding='utf-8') as file:
            reader = csv.reader(file, delimiter=';')
            for line in reader:
                Level.objects.create(pk=line[0], name=line[1])

        with open('qcm/management/commands/categories.csv', 'r', encoding='utf-8') as file:
            reader = csv.reader(file, delimiter=';')
            for line in reader:
                Category.objects.create(code=line[0], level_id=line[1], name=line[2])



