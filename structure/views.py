from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from .models import Category, Level

from django.urls import reverse_lazy, reverse
from django.views import generic
from django.contrib.auth.models import User

from .forms import UserForm


class Admin(generic.TemplateView):
    '''
    Page présentant les opérations réservées à un administrateur
    '''
    template_name = 'qcm/admin.html'

    def get_context_data(self, **kwargs):
        context = super(Admin, self).get_context_data(**kwargs)
        context['user_list'] = User.objects.all()
        context['category_list'] = Category.objects.all()
        context['level_list'] = Level.objects.all()
        return context


class UpdateProfile(LoginRequiredMixin, generic.UpdateView):
    '''
    Modification par un utilisateur de son profil
    '''
    model = User
    form_class = UserForm

    def get_object(self):
        return User.objects.get(pk=self.request.user.pk)

    def get_success_url(self):
        return reverse_lazy('list_question')


class UpdateUser(UserPassesTestMixin, generic.UpdateView):
    '''
    Edition des profils utilisateurs par un administrateur
    '''
    model = User
    form_class = UserForm

    def get_object(self):
        user_pk = self.kwargs['pk']
        return User.objects.get(pk=user_pk)

    def get_success_url(self):
        return reverse_lazy('admin')

    def test_func(self):
        return self.request.user.is_superuser


class DeleteUser(UserPassesTestMixin, generic.DeleteView):
    '''
    Suppression d'un utilisateur par un administrateur
    '''
    model = User

    def get_success_url(self):
        return reverse_lazy('admin')

    def test_func(self):
        return self.request.user.is_superuser


class CreateCategory(UserPassesTestMixin, generic.CreateView):
    '''
    Création d''une catégorie de questions
    '''

    model = Category
    fields = '__all__'

    def get_success_url(self):
        return reverse_lazy('admin')

    def test_func(self):
        return self.request.user.is_superuser


class UpdateCategory(UserPassesTestMixin, generic.UpdateView):
    '''
    Mise à jour d'une catégorie de questions
    '''

    model = Category
    fields = '__all__'

    def get_object(self):
        category_pk = self.kwargs['pk']

        return Category.objects.get(pk=category_pk)

    def get_success_url(self):
        return reverse_lazy('admin')

    def test_func(self):
        return self.request.user.is_superuser


class DeleteCategory(UserPassesTestMixin, generic.DeleteView):
    '''
    Suppression d'une catégorie de questions par un administrateur
    '''

    model = Category

    def get_success_url(self):
        return reverse_lazy('admin')

    def test_func(self):
        return self.request.user.is_superuser

    def get_context_data(self, **kwargs):
        context = super(DeleteCategory, self).get_context_data(**kwargs)
        category_pk = self.kwargs.get('pk')
        context['questions_in_category'] = Question.objects.filter(category_id=category_pk).count()
        return context


class CreateLevel(UserPassesTestMixin, generic.CreateView):
    '''
    Création d''un niveau
    '''

    model = Level
    fields = '__all__'

    def get_success_url(self):
        return reverse_lazy('admin')

    def test_func(self):
        return self.request.user.is_superuser


class UpdateLevel(UserPassesTestMixin, generic.UpdateView):
    '''
    Mise à jour des informations d''un niveau
    '''

    model = Level
    fields = '__all__'

    def get_object(self):
        level_pk = self.kwargs['pk']
        return Level.objects.get(pk=level_pk)

    def get_success_url(self):
        return reverse_lazy('admin')

    def test_func(self):
        return self.request.user.is_superuser


class DeleteLevel(UserPassesTestMixin, generic.DeleteView):
    '''
    Suppresion d'un niveau par un administrateur
    '''

    model = Level

    def get_success_url(self):
        return reverse_lazy('admin')

    def test_func(self):
        return self.request.user.is_superuser

    def get_context_data(self, **kwargs):
        context = super(DeleteLevel, self).get_context_data(**kwargs)
        level_pk = self.kwargs.get('pk')
        context['questions_in_level'] = Question.objects.filter(level_id=level_pk).count()
        return context
