from django.db import models


class Level(models.Model):
    name = models.CharField('Niveau', max_length=32)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField('Titre', max_length=256)
    level = models.ForeignKey(Level, on_delete=models.CASCADE)
    code = models.PositiveSmallIntegerField('Code', unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['code']


