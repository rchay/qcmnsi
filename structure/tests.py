from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from structure.models import Level, Category



class LevelCreateViewTest(TestCase):
    '''
    Création d'un niveau
    '''

    @classmethod
    def setUpTestData(cls):
        User.objects.create_superuser(email='admin@example.com', username='admin', password='motdepasse')
        User.objects.create_user(email='prof@example.com', username='prof', password='motdepasse')

    def test_call_view_denies_anonymous(self):
        response = self.client.get('/level/create/')
        self.assertRedirects(response, '/login/?next=/level/create/', status_code=302, target_status_code=200)
        response = self.client.get(reverse('create_level'))
        self.assertRedirects(response, '/login/?next=/level/create/', status_code=302, target_status_code=200)

    def test_call_view_denies_prof(self):
        self.client.login(username='prof', password='motdepasse')
        response = self.client.get(reverse('create_level'))
        self.assertEqual(response.status_code, 403)

    def test_call_view_loads(self):
        self.client.login(username='admin', password='motdepasse')
        response = self.client.get(reverse('create_level'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'structure/level_form.html')

    def test_call_view_fails_blank(self):
        self.client.login(username='admin', password='motdepasse')
        response = self.client.post(reverse('create_level'), {})
        self.assertFormError(response, 'form', 'name', 'Ce champ est obligatoire.')

    def test_call_view_success(self):
        self.client.login(username='admin', password='motdepasse')
        new_level = {'name': 'premiere',
                     }
        response = self.client.post(reverse('create_level'), new_level)
        self.assertEqual(Level.objects.all().count(), 1)
        self.assertRedirects(response, reverse('admin'), status_code=302, target_status_code=200)


class CategoryCreateViewTest(TestCase):
    '''
    Création d'une catégorie
    '''

    @classmethod
    def setUpTestData(cls):
        User.objects.create_superuser(email='admin@example.com', username='admin', password='motdepasse')
        User.objects.create_user(email='prof@example.com', username='prof', password='motdepasse')
        Level.objects.create(pk=0, name='Premiere')

    def test_call_view_denies_anonymous(self):
        response = self.client.get('/category/create/')
        self.assertRedirects(response, '/login/?next=/category/create/', status_code=302, target_status_code=200)
        response = self.client.get(reverse('create_category'))
        self.assertRedirects(response, '/login/?next=/category/create/', status_code=302, target_status_code=200)

    def test_call_view_denies_prof(self):
        self.client.login(username='prof', password='motdepasse')
        response = self.client.get(reverse('create_category'))
        self.assertEqual(response.status_code, 403)

    def test_call_view_loads(self):
        self.client.login(username='admin', password='motdepasse')
        response = self.client.get(reverse('create_category'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'structure/category_form.html')

    def test_call_view_fails_blank(self):
        self.client.login(username='admin', password='motdepasse')
        response = self.client.post(reverse('create_category'), {})
        self.assertFormError(response, 'form', 'name', 'Ce champ est obligatoire.')
        self.assertFormError(response, 'form', 'level', 'Ce champ est obligatoire.')
        self.assertFormError(response, 'form', 'code', 'Ce champ est obligatoire.')

    def test_call_view_success(self):
        self.client.login(username='admin', password='motdepasse')
        new_category = {'name': 'algorithme',
                        'level' : 0,
                        'code' : 11,
                     }
        response = self.client.post(reverse('create_category'), new_category)
        self.assertEqual(Category.objects.all().count(), 1)
        self.assertRedirects(response, reverse('admin'), status_code=302, target_status_code=200)
