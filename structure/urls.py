from django.urls import path, reverse_lazy, include
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('profs/password/reset/', auth_views.PasswordResetView.as_view(template_name='registration/password_reset_form.html'), name='password_reset'),
    path('profs/password/reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('profs/reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('profs/reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    path('profs/admin/', views.Admin.as_view(), name='admin'),
    path('profs/profile/', views.UpdateProfile.as_view(), name='update_profile'),
    path('profs/<int:pk>/update/', views.UpdateUser.as_view(), name='update_user'),
    path('profs/<int:pk>/delete/', views.DeleteUser.as_view(), name='delete_user'),
    path('category/create/', views.CreateCategory.as_view(), name='create_category'),
    path('category/<int:pk>/update/', views.UpdateCategory.as_view(), name='update_category'),
    path('category/<int:pk>/delete/', views.DeleteCategory.as_view(), name='delete_category'),
    path('level/create/', views.CreateLevel.as_view(), name='create_level'),
    path('level/<int:pk>/update/', views.UpdateLevel.as_view(), name='update_level'),
    path('level/<int:pk>/delete/', views.DeleteLevel.as_view(), name='delete_level'),
]
