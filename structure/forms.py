from django import forms

from django.contrib.auth.models import User


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'is_staff']
        labels = {'is_staff': 'Admin :', 'email': 'Email'}
        widgets = {'first_name': forms.TextInput(attrs={'class': 'input', 'type': 'text'}),
                   'last_name': forms.TextInput(attrs={'class': 'input', 'type': 'text'}),
                   'email': forms.TextInput(attrs={'class': 'input', 'type': 'email'}),
                   }

