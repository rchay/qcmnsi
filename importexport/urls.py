from django.urls import path, reverse_lazy, include
from . import views

urlpatterns = [
    path('question/import/', views.ImportQuestion.as_view(), name='import_question'),
    path('question/import/csv/', views.import_csv, name='import_question_csv'),
    path('question/import/xml', views.import_xml, name='import_question_xml'),
    path('question/export/', views.ExportQuestion.as_view(), name='export_question'),
    path('question/export/csv', views.ExportQuestionCSV, name='export_question_csv'),
    path('question/export/xml', views.ExportQuestionXML, name='export_question_xml'),
]
