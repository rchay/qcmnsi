from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views import generic
from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.http import HttpResponse

from .models import ImportRecord, ExportRecord
from qcm.models import Question
from qcm.constants import *



@login_required
def import_csv(request):
    '''
    Import de questions à partir d'un fichier CSV
    '''

    record = ImportRecord.objects.create(file_format=CSV,
                                         file_name=request.FILES['csv_file'].name,
                                         user=request.user)
    record.importCSV(request)

    messages.add_message(
        request,
        messages.SUCCESS,
        "L'importation des questions a été réalisée avec succès.",
        extra_tags='Importation réussie')

    return redirect(reverse_lazy('import_question'))


@login_required
def import_xml(request):
    '''
    Import de questions à partir d'un fichier XML
    '''

    record = ImportRecord.objects.create(file_format=XML,
                                         file_name=request.FILES['xml_file'].name,
                                         user=request.user)
    record.importXML(request)

    messages.add_message(
        request,
        messages.SUCCESS,
        "L'importation des questions a été réalisée avec succès.",
        extra_tags='Importation réussie')

    return redirect(reverse_lazy('import_question'))


class ExportQuestion(LoginRequiredMixin, generic.ListView):
    '''
    Page pour exporter la base de questions sous différents formats et voir
    l'historique des opérations d'exportations de questions
    '''
    template_name = 'qcm/question_export.html'
    model = ExportRecord

    def get_queryset(self):
        return ExportRecord.objects.all()


@login_required
def ExportQuestionCSV(request):
    '''
    Exporter toutes les questions au format CSV
    '''
    # todo : ajouter message erreur si l'exportation ne fonctionne pas
    questions = Question.objects.all()
    export = ExportRecord.objects.create(
        format=CSV,
        user=request.user)
    export.exportCSV(questions)

    response = HttpResponse(export.exportCSV(questions), content_type='text/csv')
    filename = f'{export.pk}_sujet'
    response['Content-Disposition'] = f'attachment; filename={filename}.csv'
    return response

    messages.add_message(
        request,
        messages.SUCCESS,
        "L'exportation des questions a été réalisée avec succès.",
        extra_tags="Exportation réussie")

    return redirect(reverse_lazy('export_question'))


@login_required
def ExportQuestionXML(request):
    '''
    Exporter toutes les questions au format XML
    '''

    questions = Question.objects.all()
    export = ExportRecord.objects.create(
        format=XML,
        user=request.user)
    export.exportXML(questions)

    messages.add_message(
        request,
        messages.SUCCESS,
        "L'exportation des questions a été réalisée avec succès.",
        extra_tags="Exportation réussie")

    return redirect(reverse_lazy('export_question'))


class ImportQuestion(LoginRequiredMixin, generic.ListView):
    '''
    Page pour importer des questions à partir d'un fichier et voir
    l'historique des opérations d'import de fichiers
    '''
    template_name = 'qcm/question_import.html'
    model = ImportRecord
    paginate_by = 10

    def get_queryset(self):
        return ImportRecord.objects.select_related('user').all().prefetch_related('question_set')
