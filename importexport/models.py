import time
import csv
import re
import io

from django.db import models
from django.apps import apps
from django.core.files import File

from django.contrib.auth.models import User

from os.path import join
from structure.models import Category
from xml.etree.ElementTree import Element, SubElement, Comment, tostring, parse, fromstring
from xml.dom import minidom

from qcm.constants import *
from structure.models import Level, Category
from qcmnsi import settings

def prettify(elem):
    ''' Mise en forme et indentation de l'export XML '''
    rough_string = tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


def cdata(text):
    ''' entoure le texte par des balises CDATA pour échapper le html dans pronote'''
    return f'<![CDATA[{text}]]>'


def add_div(text):
    ''' entoure une ligne dans des balises <div></div>'''
    return f'<div style="font-family: Arial; font-size: 13px;">{text}</div>'


def add_span(text):
    ''' entoure le code dans des balises <span></span>'''
    return f'''<span style="font-family: 'courier new', courier;">{text}</span>'''


def format_code(text):
    '''
    remplace les balises <python></python> et autres par des balises <span></span> pour
    l'affichage HTML du code dans pronote
    '''
    for tag in ['tt', 'python', 'js']:
        for block_tags in re.findall(f'(<{tag}>([\w\W]+?)</{tag}>)', text):
            code = block_tags[1].split('\n')
            formatted_code = ''
            for line in code:
                formatted_code += add_span(line)
                if len(code) > 1:
                    formatted_code += '\n'
            text = text.replace(block_tags[0], formatted_code)
    return text


def remove_div(text):
    '''
    enlève les balises html
    '''
    for tag in ['span', 'p', 'div']:
        for block_tags in re.findall(f'(<{tag}[^<>]*>)', text):
            text = text.replace(block_tags, '')
        for block_tags in re.findall(f'(</{tag}[^<>]*>)', text):
            text = text.replace(block_tags, '')
    return text


def format_pronote(text):
    text = text.replace("<br>", "\n")
    text = text.replace("  ", "\xa0\xa0")
    text = text.replace("&nbsp;", "\xa0")
    text = format_code(text)
    result = ''
    for line in text.split('\n'):
        result += add_div(line)
    return cdata(result)


class ImportRecord(models.Model):
    '''
    Enregistrement d'une opération d'import de questions
    '''

    created = models.DateTimeField(auto_now_add=True)
    file_format = models.PositiveSmallIntegerField('Format', choices=CHOICES_FILE_FORMAT)
    file_name = models.CharField('Nom de fichier', max_length=512)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-created']

    def importCSV(self, request):

        # lecture par chunk pour ne pas surcharger la mémoire
        uploaded_file_content = ''
        for chunk in request.FILES['csv_file'].chunks():
            uploaded_file_content += str(chunk.decode('utf-8'))

        timestamp = round(time.time())
        filename = f'import-{timestamp}.csv'
        folder = 'import/'
        filepath = f'{folder}{filename}'

        # enregistre le fichier sur disque
        with open(filepath, 'w', encoding='utf-8', newline='') as write_file:
            write_file.write(uploaded_file_content)

        Question = apps.get_model('qcm', 'Question')

        # ouvre le fichier
        with open(filepath, 'r', encoding='utf-8', newline='') as read_file:
            reader = csv.DictReader(read_file, delimiter=';')
            for line in reader:
                category = Category.objects.get(code=line['categorie'])
                q = Question(
                    category=category,
                    level=category.level,
                    text=line['enonce'],
                    answer1=line['reponse_1'],
                    answer2=line['reponse_2'],
                    answer3=line['reponse_3'],
                    answer4=line['reponse_4'],
                    author=line['auteur'],
                    import_record=self,
                    user=request.user,
                )
                try:
                    q.save()
                    for tag in line['tags'].split('|'):
                        if tag != '':
                            q.tags.add(tag)
                except KeyError:
                    print('error')
                    pass
                print(line['tags'], 'split', 'tags'.split('|'))

    def importXML(self, request):

        # lecture par chunk pour ne pas surcharger la mémoire
        uploaded_file_content = ''
        for chunk in request.FILES['xml_file'].chunks():
            uploaded_file_content += str(chunk.decode('utf-8'))
        uploaded_file_content = uploaded_file_content.replace('<br>', '\n')
        uploaded_file_content = uploaded_file_content.replace('&npsp;', ' ')

        Question = apps.get_model('qcm', 'Question')

        tree = fromstring(uploaded_file_content)
        question_list = []
        for question in tree.iter('question'):
            question_type = question.attrib['type']
            if question_type == 'multichoice':
                parsed_question = {}
                parsed_question['wrong'] = []
                for child in question.iter():
                    tag = child.tag

                    if tag == 'answer':
                        ans = remove_div(child.find('text').text.strip())
                        if child.attrib['fraction'] == '100':
                            parsed_question['right'] = ans
                        else:
                            parsed_question['wrong'].append(ans)
                    elif tag == 'name':
                        parsed_question['name'] = child.find('text').text.strip()
                    elif tag == 'questiontext':
                        parsed_question['text'] = remove_div(child.find('text').text.strip())

                question_list.append(parsed_question)

            elif question_type == 'category':
                for child in question.iter('text'):

                    obj = re.search('(<name>([\w\W]+?)</name>)', child.text)
                    if obj:
                        quiz_title = obj.group(2)

        for question in question_list:
            q = Question(
                category=Category.objects.first(),
                level=Level.objects.first(),
                text=question['text'],
                answer1=question['right'],
                answer2=question['wrong'][0],
                answer3=question['wrong'][1],
                answer4=question['wrong'][2],
                author='import xml',
                import_record=self,
                user=request.user,
            )
            try:
                q.save()
            except KeyError:
                pass
            # print(line['tags'], 'split', 'tags'.split('|'))


class ExportRecord(models.Model):
    '''
    Enregistrement d'une opération d'export de questions
    '''

    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    format = models.PositiveSmallIntegerField('Format', choices=CHOICES_FILE_FORMAT)
    filepath = models.CharField('Titre ', max_length=200, null=True)
    question_number = models.PositiveSmallIntegerField('Nombre de questions', null=True)
    file = models.FileField('Fichier', null=True)

    class Meta:
        ordering = ['-created']

    def exportCSV(self, questions):
        timestamp = round(time.time())
        filename = f'export-{timestamp}.csv'
        folder = 'export'
        filepath = join(settings.MEDIA_ROOT, folder, filename)
        question_number = 0

        output = io.StringIO()

        writer = csv.writer(output, delimiter=';')
        writer.writerow(['categorie', 'enonce', 'reponse_1', 'reponse_2', 'reponse_3', 'reponse_4',
                         'auteur', 'tags'
                         ])
        for q in questions:
            try:
                code = q.category.code
            except AttributeError:
                code = ""
            writer.writerow([
                code,
                q.text,
                q.answer1,
                q.answer2,
                q.answer3,
                q.answer4,
                q.author,
                '|'.join(list(q.tags.names())),

            ])
            question_number += 1

        self.file = filepath
        self.question_number = question_number
        self.filepath = filepath
        self.save()

        return output.getvalue()

    def exportXML(self, questions):
        timestamp = round(time.time())
        filename = f'export-{timestamp}.xml'
        folder = 'export/'
        filepath = f'{folder}{filename}'
        question_number = 0

        root = Element('quiz')

        question = SubElement(root, 'question', {'type': 'category'})
        category = SubElement(question, 'category')
        text = SubElement(category, 'text')
        text.text = "export XML"

        for q in questions:
            question = SubElement(root, 'question', {'type': 'multichoice'})

            name = SubElement(question, 'name')
            text = SubElement(name, 'text')
            text.text = f'Question {question_number}'

            questiontext = SubElement(question, 'questiontext', {'format': 'html'})
            text = SubElement(questiontext, 'text')
            text.text = format_pronote(q.text)

            defaultgrade = SubElement(question, 'defaultgrade')
            defaultgrade.text = "1"

            single = SubElement(question, 'single')
            single.text = "true"

            for i in q.answer_order:
                if i == 1:
                    fraction = 100
                    a = format_pronote(q.answer1)
                else:
                    fraction = 0
                    if i == 2:
                        a = format_pronote(q.answer2)
                    elif i == 3:
                        a = format_pronote(q.answer3)
                    elif i == 4:
                        a = format_pronote(q.answer4)

                answer = SubElement(question, 'answer', {'format': 'html', 'fraction': str(fraction)})
                text = SubElement(answer, 'text')
                a = a.replace('\n', '<br>')
                text.text = a
            question_number += 1

        output = prettify(root)
        output = output.replace('&lt;', '<')
        output = output.replace('&gt;', '>')
        output = output.replace('&quot;', '"')

        with open(filepath, 'w', encoding='utf-8', newline='') as xmlfile:
            xmlfile.write(output)

        self.question_number = question_number
        self.filepath = filepath
        self.save()
