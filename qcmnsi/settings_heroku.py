# Fichier de configuration pour le déploiement sur heroku

from qcmnsi.settings import *
import django_heroku
import os

print('Loading heroku production settings')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY')

ALLOWED_HOSTS = ['parcours.erlm.tn']

# info de connexion au serveur mail
....

django_heroku.settings(locals())