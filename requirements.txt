Django==3.0.5
django-debug-toolbar==2.1
django-taggit==1.1.0
Pygments==2.4.2
pytz==2019.3
sqlparse==0.3.0
django-tex==1.1.7
gunicorn
django-heroku