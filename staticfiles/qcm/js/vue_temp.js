Vue.component('question', {
    props: ['quiz', 'question', 'submission', 'number'],
    data: function() {
        return {
            answered: false,
            selected: null,
            answer: null,
            key: null,
        }
    },
    methods: {
        sendAnswer: function(answer) {
            axios
                .get('/ajax/', {
                    params: {
                        'answer': answer,
                        'submission': this.submission,
                        'question': this.question.pk,
                    }
                })
                .then(response => (this.answered = true))
        }
    },

  template: `<div class="card" style="margin-bottom:20px;"><div class="card-content">
    <div class="media">
        <div class="media-content">
            <p class="title is-4 is-pulled-left">Question {{number}}</p>
        </div>
    </div>
    <div class="content">
        <div v-html=question.text></div>
    </div>
    <div class="content">
        <div class="field">
            <div class="input" style="height: auto;"
                 v-on:click="sendAnswer(1)"
                 v-bind:class="{ 'is-primary': selected == '1','is-focused': selected == '1' }" @click="selected = '1'">
                <div class="content" style="width: 100%;">
                    <label>
                        <div v-html=question.answer1></div>
                    </label>
                </div>
            </div>
        </div>

    <div class="field">
        <div class="input" style="height: auto;"
        v-on:click="sendAnswer(2)"
        v-bind:class="{ 'is-primary': selected == '2','is-focused': selected == '2' }" @click="selected = '2'">
            <div class="content" style="width: 100%;">
                <label>
                    <div v-html=question.answer2></div>
                </label>
            </div>
        </div>
    </div>
    <div class="field">
        <div class="input" style="height: auto;"
        v-on:click="sendAnswer(3)"
        v-bind:class="{ 'is-primary': selected == '3' ,'is-focused': selected == '3'}" @click="selected = '3'">
            <div class="content" style="width: 100%;">
                <label>
                    <div v-html=question.answer3></div>
                </label>
            </div>
        </div>
    </div>
    <div class="field">
        <div class="input" style="height: auto;"
         v-on:click="sendAnswer(4)"
         v-bind:class="{ 'is-primary': selected == '4' ,'is-focused': selected == '4'}" @click="selected = '4'">
            <div class="content" style="width: 100%;">
                <label>
                    <div v-html=question.answer4></div>
                </label>
            </div>
        </div>
    </div>
    <div class="field">
        <div class="input" style="height: auto;"
        v-on:click="sendAnswer(5)"
        v-bind:class="{ 'is-active': selected == '5' }" @click="selected = '5'">
            <div class="content" style="width: 100%;">
                <label>
                    Ne pas répondre
                </label>
            </div>
        </div>
    </div></div>
</div></div>
`
})


var app = new Vue({
    el: '#app',
    delimiters: ['[[', ']]'],
    data: {
        questions: null,
        submission: null,
    },
    methods: {
        init() {
            submission = document.getElementById('app').dataset.submission
            axios
                .get('/ajax2/', {
                    params: {
                        'submission': submission
                    }
                })
                .then(response => (this.questions = JSON.parse(response.data.questions)))
                .then(function () {MathJax.typesetPromise()})
        },
    },
    mounted: function() {
        this.init()
    }
})


